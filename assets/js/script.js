/*
ES6
	ECMAScript
		- a standard for scripting languages like JavaScript
		- ES5 
			- current version implemented into most browsers
	1. let and const
	2. Destructuring
		- allows us to break apart key structure into variable
		- Array Destructuring
*/
let employee = ['Sotto', 'Tito', 'Senate President', 'Male'];

let [lastName, firstName, position, gender] = employee;//one to one mapping

console.log(lastName);
console.log(position);

let player2 = ['Curry', 'Lillard', 'Paul', 'Irving'];

const [pointGuard1, pointGuard2, pointGuard3, pointGuard4] = player2;
console.log(pointGuard1);
console.log(pointGuard2);
console.log(pointGuard3);
console.log(pointGuard4);

// object destructuring - it allows us to destructure an object by allowing to add the values of an object's property into respective variables

let pet = {
	species: 'dog',
	color: 'brown',
	breed: 'German Sheperd'
};

// let {breed, color} = pet;
// alert(`I have ${breed} dog and it's color is ${color}`);
getPet({
	species: 'dog',
	color: 'black',
	breed: 'Doberman'
})


// function getPet(options){
// 	alert(`I have ${options.breed} dog and it's color is ${options.color}`);
// };

// function getPet(options){
// 	let breed = options.breed;
// 	let color = options.color;

// 	console.log(`I have ${breed} dog and it's color is ${color}`);
// };


// function getPet(options){
// 	let {breed, color} = options;
// 	console.log(`I have ${breed}dog and it's color is ${color}`);
// }

function getPet({breed, color}){
	console.log(`I have ${breed}dog and it's color is ${color}`);
};

// without function
let getPet2 = {
	species: 'dog',
	color: 'brown',
	breed: 'German Sheperd'
};

let {breed, color} = getPet2;
console.log(`I have ${breed}dog and it's color is ${color}`);

/*
	let person = {
		name: <name>,
		birthday: <bday>,
		age: <age>
	}

	Create 3 new sentence variable which will contain the following strings:
		sentence1: Hi, I'm <personName>,
		sentence2: I was born on <personBday>,
		sentence3: I am <personAge> years old

	Display the three sentences in console or in alert
 */
let person = {
		name: 'Paul Phoenix',
		birthday: 'August 5, 1995',
		age: 26
	};
let sentence1 = `Hi, I'm ${person.name}`;
let sentence2 =`I was born on ${person.birthday}`;
let sentence3 =`I am ${person.age} years old.`;

console.log(sentence1);
console.log(sentence2);
console.log(sentence3);

const {age, name, birthday} = person;

console.log(age);
console.log(name);
console.log(birthday);

let pokemon1 = {
	name1: 'Charmander',
	level: 15,
	type: 'Fire',
	moves: ['ember', 'scratch','leer']
}

const {name1, level, type, moves} = pokemon1;
let sentence4 = `My pokemon is ${name1}. It is level ${level}. It is a ${type} and its movements are ${moves}.`

// what data type is moves?
console.log(moves);

const [move1,,move3] = moves;
console.log(move1);
console.log(move3);

pokemon1.name1 = 'Bulbasaur';
console.log(name1);
console.log(pokemon1);

// Arrow Functions - fat arrow
// before ES6
// function printFullName (firstName, middleInitial, lastName){
// 	return firstName + ' ' + middleInitial + ' ' + lastName;
// };

// // In ES6
// const printFullName = (firstName, middleInitial, lastName) => {
// 	return firstName + ' ' + middleInitial + ' ' + lastName;
// };

// traditional function
function displayMsg(){
	console.log(`Hello world!`);
};


const hello = () => {
	console.log(`Hello from arrow.`);
};

hello();

// function greet(pokemon1){
// 	console.log(`Hi, ${pokemon1.name1}!`)
// };
// greet(pokemon1)

const greet = (pokemon1) => {
	console.log(`Hi, ${pokemon1.name1}!`);
};
greet(pokemon1);

// Implicit Return - allows us to return a value without the use of return keyword.

// function addNum(num1, num2){
// 	console.log(num1 + num2);
// };

const addNum = (num1, num2) => num1 + num2;
let sum = addNum(5, 6);
console.log(sum);

// this keyword
let protagonist = {
	name: 'Cloud strife',
	occupation: 'Soldier',
	greet : function(){
		// traditional methods would have this keyword refer to the parent object
		console.log(this);
		console.log(`Hi! I'm ${this.name}.`)
	},
	introduceJob: () => {
		console.log(this);
		console.log(`I work as ${protagonist.occupation}.`);
	}
}

protagonist.greet();
protagonist.introduceJob();

// Class-base Object Blueprints
	//Classes are template of objects

	// Create a class
			//The constructor is a special method for creating and initializing an object
	function Pokemon(name, type, level){
		this.name = name;
		this.type = type;
		this.level = level;
	};

	// ES6 Class Creation
	class Car {
		constructor(brand, name, year){
			this.brand = brand;
			this.name = name;
			this.year = year;
		}
	};

	let car1 = new Car('Toyota', 'Vios', '2002');
	console.log(car1);
	let car2 = new Car('Cooper', 'Mini', '1969');
	console.log(car2);
	let car3 = new Car('Porsche', '911', '1960');
	console.log(car3);

/*
Activity:
1. Update and Debug the following codes to ES6
		Use template literals
		Use array/object destructuring
		Use arrow function
2. Create a class constructor able to receive 3 arguments
		- it should be able to receive two strings and a number
		- Using the this keyword assign properties:
			name,
			breed,
			dogAge = <7 * human years>
				- assign the parameters as values to each property.
3. Create 2 new objects using our class constructor
	This constructor should be able to create Dog objects.

	Log the 2 new Dog objects in the console or alert.

*/
let student1 = {
	name: "Shawn Michaels", 
	birthday: "May 5, 2013",
	age: 18,
	isEnrolled: true,
	classes: ['Philosphy 101', 'Social Sciences 201'], 
}

let student2 = {
	name: "Steve Austin",
	birthday: "June 15, 2001",
	age: 20, 
	isEnrolled: true, 
	classes: ['Philosphy 401' , 'Natural Sciences 402'] 
}


introduce(student1);
introduce(student2);
function introduce(student){

  const {name, birthday,  age, isEnrolled, classes} = student;
	//Note: You can destructure objects inside functions.
  
	console.log(`Hi! I'm ${name} I am ${age} years old.`);
	console.log(`I study the following courses ${classes}`);
}


const getCube = (num) => Math.pow(num,3);
getCube(5)
console.log(getCube);

let cube = getCube(3);

console.log(cube)

let numArr = [15,16,32,21,21,2]

numArr.forEach(function(num){

	console.log(num);
})

let numsSquared = numArr.map(function(num){

	return num ** 2;

})

console.log(numsSquared);

class Dog {
    constructor(name, breed, dogAge){
   this.name = name;
   this.breed = breed;
   this.dogAge = dogAge;
} 
};


let dog1 = new Dog('Browny', 'Golden Retriever', '5');
console.log(dog1);

let dog2 = new Dog('Loki', 'Shiba inu', '3');
console.log(dog2);
